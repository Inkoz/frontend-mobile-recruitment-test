import React from 'react';
import {server} from './api/mocks/server';
import {AuthProvider} from './utils/context/auth/auth.context';
import {AppNavigation} from './routes/AppNavigation';

// start msw server for mocks, don't remove
server.listen({onUnhandledRequest: 'bypass'});

const App = () => {
  return (
    <AuthProvider>
      <AppNavigation />
    </AuthProvider>
  );
};

export default App;
