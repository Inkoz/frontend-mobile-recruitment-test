# SRC

The entry point is `App.tsx`, this should be where you configure libs, add providers and initialise code for all the app

## [Api](./api/README.md)

## [Routes](./routes/README.md)

## [Screens](./screens/README.md)

## [Ui](./ui/README.md)

## [Utils](./utils/README.md)
