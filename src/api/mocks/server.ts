import {setupServer} from 'msw/native';
import {getAuthenticationMSW} from './authentication.msw';
import 'react-native-url-polyfill/auto';
import {getNotesMsw} from './notes.msw';

export const server = setupServer(...getAuthenticationMSW(), ...getNotesMsw());
