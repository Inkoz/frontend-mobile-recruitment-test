import {rest} from 'msw';
import {faker} from '@faker-js/faker';
import {Note} from '../models/notes';

const getNote = (): Note => ({
  title: faker.random.words(faker.datatype.number({min: 1, max: 6})),
  id: faker.datatype.uuid(),
  description: faker.lorem.lines(),
});

export const getNotesMsw = () => [
  rest.get('http://mock.api/api/notes', (req, res, ctx) => {
    return res(
      ctx.delay(200),
      ctx.status(200, 'Mocked status'),
      ctx.json(
        faker.helpers.uniqueArray(
          getNote,
          faker.datatype.number({
            min: 5,
            max: 20,
          }),
        ),
      ),
    );
  }),
  rest.get('http://mock.api/api/notes/:id', (req, res, ctx) => {
    if (!req.params.id) {
      return res(ctx.status(401));
    }

    return res(
      ctx.delay(200),
      ctx.status(200, 'Mocked status'),
      ctx.json(getNote()),
    );
  }),
];
