# API

## Endpoints

List of util functions for endpoints that you can use in your components

Contains:

- authentication
    - POST: login
    - POST: logout
- notes
    - GET: all notes
    - GET: one note

## Mocks

Since we don't have a real backend in a server, we use `msw` to mock everything.\
All request to the api are intercepted by `msw` and fake data is returned with `@faker-js/faker`

> You don't need to modify anything inside this folder

## Models

Typed schemas of the response from the server for the `notes` endpoints.

> You don't need to modify anything inside this folder
