import {AxiosResponse} from 'axios';

export type Identity = {
  email: string;
};

export type AuthContextState = {
  isLoggedIn: boolean;
  isLoading: boolean;
  login: (body: {email: string; password: string}) => Promise<AxiosResponse>;
  logout: () => Promise<AxiosResponse>;
  identity: Identity | null;
};
