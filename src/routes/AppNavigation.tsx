import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {LoginScreen} from '../screens/login/LoginScreen';
import {HomeScreen} from '../screens/home/HomeScreen';
import {NoteDetailsScreen} from '../screens/note-details/NoteDetailsScreen';
// TODO handle routing with `react-navigation`
type RootStackParamList={
  LoginScreen:undefined;
  HomeScreen:{userId : string};
  NoteDetailsScreen:undefined;
}
const RootStack = createNativeStackNavigator<RootStackParamList>();

export const AppNavigation = () => {
  // TODO There are three routes: `Login`, `Home`, `NoteDetails`
  return (
    <NavigationContainer>
      <RootStack.Navigator initialRouteName="LoginScreen">
        <RootStack.Screen name="LoginScreen" component={LoginScreen} />
        <RootStack.Screen name="HomeScreen"  component={HomeScreen} />
        <RootStack.Screen name="NoteDetailsScreen" component={NoteDetailsScreen} />
      </RootStack.Navigator>
    </NavigationContainer>
  );
};
